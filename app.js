

var Jill = {
  appVersion: "0.1.8",
  baseUrl: 'http://training-jac-1.getsandbox.com'
};

Jill.Users = {
  route: '/users',
  getRoute: function(){ return Jill.baseUrl + this.route }
};

Jill.Users.Login = {
  bind: function () {
    $('.login.button').on('click', function () {
      var formData = {
        username: $('.login .username').val(),
        password: $('.login .password').val()
      };
      $.ajax({
        type: 'POST',
        url: Jill.Users.getRoute() + '/' + formData.username,
        data: { password: formData.password },
        dataType: 'json',
        context: $('body'),
        success: function (data) {
          alert('Succeeded!');
        },
        error: function (xhr, type) {
          alert('Login error! ' + type);
        }
      });
    });
  },
  submit: function () { }
};

Jill.Users.Register = {
  bind: function () {
    $('.register.button').on('click', function () {
      var formData = {
        username: $('.register .username').val(),
        password: $('.register .password').val(),
        name: $('.register .name').val(),
        isAdmin: $('.register .isAdmin').is(':checked')
      };
      $.ajax({
        type: 'PUT',
        url: Jill.Users.getRoute(),
        data: formData,
        dataType: 'json',
        context: $('body'),
        success: function (data) {
          alert('Register for '+ formData.username + ' Succeeded!');
        },
        error: function (xhr, type) {
          alert('Register failed! ' + type);
        }
      });
    });
  },
  submit: function () { }
};

Jill.start = function () {
  $(".app-version").html(Jill.appVersion);
  Jill.Users.Login.bind();
  Jill.Users.Register.bind();
}

